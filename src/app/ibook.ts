export interface Ibook {
  id: number;
  title: string;
  description: string;
  author: string;
  bookImage: string;
  category: string;
}
