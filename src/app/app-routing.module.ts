import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './components/homepage/homepage.component';
import { LoginComponent } from './components/login/login.component';
import { BookpageComponent } from './components/bookpage/bookpage.component';
import { AddbookComponent } from './components/addbook/addbook.component';
import { EachbookComponent } from './components/eachbook/eachbook.component';
import { authGuard } from './authentication/auth.guard';
import { NotFoundComponent } from './components/not-found/not-found.component';

const route: Routes = [
  {
    path: '',
    component: HomepageComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'books',
    component: BookpageComponent,
    canActivate: [authGuard],
  },
  {
    path: 'addbook',
    component: AddbookComponent,
  },
  {
    path: 'books/:id',
    component: EachbookComponent,
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forRoot(route)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
