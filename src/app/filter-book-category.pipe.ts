import { Pipe, PipeTransform } from '@angular/core';
import { Ibook } from './ibook';

@Pipe({
  name: 'filterBookCategory',
})
export class FilterBookCategoryPipe implements PipeTransform {
  transform(books: Ibook[], selectedCategory: string): Ibook[] {
    if (!selectedCategory || selectedCategory === 'All Genre') {
      return books;
    }

    return books.filter(
      (book) => book.category.toLowerCase() === selectedCategory.toLowerCase()
    );
  }
}
