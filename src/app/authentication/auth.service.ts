import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor() {}
  private tokenKey = 'authToken';

  login(email: string, password: string): boolean {
    const token = '123';
    localStorage.setItem(this.tokenKey, token);
    return true;
  }

  logout(): void {
    localStorage.removeItem(this.tokenKey);
  }

  isAuthenticated(): boolean {
    return !!localStorage.getItem(this.tokenKey);
  }
}
