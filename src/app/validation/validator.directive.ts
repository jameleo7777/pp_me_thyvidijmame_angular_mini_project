import { Directive,Input  } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, ValidationErrors } from '@angular/forms';


@Directive({
  selector: '[appValidator]',
  providers: [{ provide: NG_VALIDATORS, useExisting: ValidatorDirective, multi: true }]
})
export class ValidatorDirective {
  @Input('appCustomValidation') validationType ?: string;

  constructor() { }
  validate(control: AbstractControl): ValidationErrors | null {
    const value = control.value;

    if (this.validationType === 'email') {
      const emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
      if (!emailPattern.test(value)) {
        return { invalidEmail: true };
      }
    }

    if (this.validationType === 'password') {
      const passwordPattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
      if (!passwordPattern.test(value)) {
        return { invalidPassword: true };
      }
    }

    return null;
  }
}
