import { BookService } from 'src/app/services/book.service';
import { Component } from '@angular/core';
import { Ibook } from 'src/app/ibook';
import { ActivatedRoute, Router } from '@angular/router';
import { FilterBookCategoryPipe } from 'src/app/filter-book-category.pipe';

@Component({
  selector: 'app-bookpage',
  templateUrl: './bookpage.component.html',
  styleUrls: ['./bookpage.component.css'],
})
export class BookpageComponent {
  constructor(
    private _bookService: BookService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  bookList: Ibook[] = [];
  selectedCategory: string = 'All Genre';
  findCategoryName: string = '';
  genres: string[] = [
    'Crime Thrille',
    'Historical Fiction',
    'Coming-of-age Story',
    'Romance',
  ];

  ngOnInit() {
    this.getBooks();
  }

  getBooks() {
    this._bookService.getBookFromService().subscribe((e) => {
      console.log(e);
      this.bookList = e;
    });
  }

  searchCategoryName(category: string) {
    this.selectedCategory = category;
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: { category: category },
      queryParamsHandling: 'merge',
    });
  }
  setBookId(bookId: number) {
    for (let book of this.bookList) {
      this.router.navigate(['/books', bookId]);
    }
  }
}
