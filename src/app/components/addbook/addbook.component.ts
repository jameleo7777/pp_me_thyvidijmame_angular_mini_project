import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Ibook } from 'src/app/ibook';
import { BookService } from 'src/app/services/book.service';
import { Validator } from '@angular/forms';

export enum BookCategory {
  Romance = 'Romance',
  CrimeThriller = 'Crime Thriller',
  HistoricalFiction = 'Historical Fiction',
  ComingOfAge = 'Coming-of-age Story',
}

@Component({
  selector: 'app-addbook',
  templateUrl: './addbook.component.html',
  styleUrls: ['./addbook.component.css'],
})
export class AddbookComponent {
  categories: string[] = Object.values(BookCategory);
  bookForm!: FormGroup;
  bookList: Ibook[] = [];
  constructor(
    private formBuilder: FormBuilder,
    private _bookService: BookService,
    private http: HttpClient
  ) {
    this.bookForm = this.formBuilder.group({
      title: new FormControl('', Validators.required),
      author: new FormControl('', Validators.required),
      category: ['technology'],
      description: new FormControl('', Validators.required),
    });
  }

  ngOnInit() {}

  addNewBook() {
    console.log(this.bookForm.value);
    let bookObject: any = {
      title: this.bookForm.value.title,
      author: this.bookForm.value.author,
      category: this.bookForm.value.category,
      description: this.bookForm.value.description,
      bookImage: '/assets/images/Book2.png',
    };
    this._bookService.addFromService(bookObject).subscribe((res) => {
      console.log(res);
      this._bookService.getBookFromService();
    });
  }
}
