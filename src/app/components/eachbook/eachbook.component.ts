import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, filter, of } from 'rxjs';
import { Ibook } from 'src/app/ibook';
import { BookService } from 'src/app/services/book.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-eachbook',
  templateUrl: './eachbook.component.html',
  styleUrls: ['./eachbook.component.css'],
})
export class EachbookComponent {
  constructor(
    private _bookService: BookService,
    private activatedRoute: ActivatedRoute
  ) {}

  books: Ibook[] = [];

  book!: Ibook;
  bookId: number | undefined;

  ngOnInit(): void {
    this.bookId = +this.activatedRoute.snapshot.paramMap.get('id')!;
    this._bookService.getBookFromService().subscribe(
      (e) => {
        this.books = e;
        const foundBook = e.find((data) => this.bookId == data.id);
        if (foundBook) {
          this.book = foundBook;
          console.log(this.book);
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
