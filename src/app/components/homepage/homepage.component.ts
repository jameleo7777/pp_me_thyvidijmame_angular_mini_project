import { HttpClient } from '@angular/common/http';
import { Component, ɵSSR_CONTENT_INTEGRITY_MARKER } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ibook } from 'src/app/ibook';
import { BookService } from 'src/app/services/book.service';
import { __classPrivateFieldGet } from 'tslib';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css'],
})
export class HomepageComponent {
  constructor(
    private _bookService: BookService,
    private http: HttpClient,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  books: Ibook[] = [];
  updateBook!: Ibook;
  book!: Ibook;
  bookId: number | undefined;
  afterUpdate!: Ibook;
  isModalOpen: boolean = false;

  openModal(id: number) {
    console.log('id:', id);
    this.isModalOpen = true;
    console.log(
      this.books.filter((data) => {
        if (data.id === id) {
          this.book = data;
          console.log(this.book);
          return true;
        }
        return false;
      })
    );
    this.updateBook = { ...this.updateBook };
  }

  closeModal() {
    this.isModalOpen = false;
  }

  ngOnInit() {
    this.getBooks();
  }

  getBooks() {
    this._bookService.getBookFromService().subscribe((e) => {
      console.log(e);
      this.books = e;
    });
  }

  updateBookById(
    id: number,
    book: Ibook,
    afterUpdatedTitle: HTMLInputElement,
    afterUpdatedAuthor: HTMLInputElement,
    afterUpdatedDescription: HTMLInputElement
  ) {
    this.isModalOpen = false;

    const updatedTitle = afterUpdatedTitle.value;
    const updateAuthor = afterUpdatedAuthor.value;
    const updateDescription = afterUpdatedDescription.value;

    if (
      updatedTitle !== book.title ||
      updateAuthor !== book.author ||
      updateDescription !== book.description
    ) {
      book.title = updatedTitle;
      book.author = updateAuthor;
      book.description = updateDescription;
      console.log(this.book.id);
      console.log(this.book);
      let datA = {
        id: id,
        title: book.title,
        description: book.description,
        author: book.author,
        bookImage: this.book.bookImage,
        category: this.book.category,
      };

      console.log('GGG: ', datA);
      this._bookService.updateFromService(datA, id).subscribe(
        (response) => {
          console.log('success');
          this.getBooks();
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  setBookId(bookId: number) {
    for (let book of this.books) {
      this.router.navigate(['/books', bookId]);
    }
  }

  removeBook(product: Ibook) {
    const id = product.id;
    console.log(product);
    this._bookService
      .deleteFromService(id)
      .subscribe((product) => console.log(product));
    this.getBooks();
  }
}
