import { Component } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/authentication/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  loginForm!: FormGroup;
  constructor(private login: FormBuilder, private authService: AuthService) {
    this.loginForm = this.login.group({
      email: [''],
      password: [''],
    });
  }

  isLogin: boolean = false;

  loginTo() {
    this.isLogin = true;
    if (this.loginForm.valid) {
      const email = this.loginForm.get('email')?.value;
      const password = this.loginForm.get('password')?.value;

      if (this.authService.login(email, password)) {
        const authToken = Math.random.toString();

        localStorage.setItem('authToken', authToken);

        console.log('Successfully');
      } else {
        console.log('Fail');
      }
    }
  }

  logout() {
    this.isLogin = false;
    this.authService.logout();
  }
}
