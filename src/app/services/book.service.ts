import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Ibook } from '../ibook';

@Injectable({
  providedIn: 'root',
})
export class BookService {
  URL = 'api/books/';
  constructor(private http: HttpClient) {}

  getBookFromService(): Observable<Ibook[]> {
    return this.http.get<Ibook[]>(this.URL);
  }

  deleteFromService(id: number): Observable<Ibook> {
    return this.http.delete<Ibook>(`${this.URL + id}  `);
  }

  updateFromService(book: Ibook, id: number): Observable<Ibook> {
    const url = `${this.URL}${id}`;
    console.log('Update URL:', url); // Log the constructed URL
    return this.http.put<Ibook>(url, book);
    // return this.http.put<Ibook>(this.URL, book,);
  }

  addFromService(book: Ibook): Observable<Ibook> {
    return this.http.post<Ibook>(this.URL, book);
  }
}
