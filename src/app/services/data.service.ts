import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  InMemoryBackendConfig,
  InMemoryDbService,
} from 'angular-in-memory-web-api';
import { Ibook } from '../ibook';
@Injectable({
  providedIn: 'root',
})
export class DataService implements InMemoryDbService {
  constructor() {}
  createDb() {
    let books: Ibook[] = [
      {
        id: 1,
        title: 'Power of sadness',
        description:
          'Whether reading the short or long description, both need to be full of what readers want: intrigue. It’s up to you to get readers interested in what you have to say, and while we know ',
        author: 'Jame',
        bookImage: '/assets/images/Book1.png',
        category: 'Romance',
      },
      {
        id: 2,
        title: 'Power of Love',
        description:
          'Whether reading the short or long description, both need to be full of what readers want: intrigue. It’s up to you to get readers interested in what you have to say, and while we know ',
        author: 'Jimmy',
        bookImage: '/assets/images/Book2.png',
        category: 'Crime Thrille',
      },
      {
        id: 3,
        title: 'Money Change the world',
        description:
          'Whether reading the short or long description, both need to be full of what readers want: intrigue. It’s up to you to get readers interested in what you have to say, and while we know ',
        author: 'Rady',
        bookImage: '/assets/images/Book3.png',
        category: 'Historical Fiction',
      },
      {
        id: 4,
        title: 'Destruction of the world',
        description:
          'Whether reading the short or long description, both need to be full of what readers want: intrigue. It’s up to you to get readers interested in what you have to say, and while we know ',
        author: 'Vannak',
        bookImage: '/assets/images/Book4.png',
        category: 'Historical Fiction',
      },
      {
        id: 5,
        title: 'New Style',
        description:
          'Whether reading the short or long description, both need to be full of what readers want: intrigue. It’s up to you to get readers interested in what you have to say, and while we know ',
        author: 'Trimil',
        bookImage: '/assets/images/Book5.png',
        category: 'Coming-of-age Story',
      },
      {
        id: 6,
        title: 'Despost',
        description:
          '  Lorem ipsum, dolor sit amet consectetur adipisicing           dolor sit amet consectetur adipisicing elit. Sunt ipsum reprehenderit dolor aspernatur voluptatem. Autem,eveniet minus iste eum quos sint iusto aut ',
        author: 'Demeler',
        bookImage: '/assets/images/Book6.png',
        category: 'Coming-of-age Story',
      },
    ];
    return { books };
  }
}
