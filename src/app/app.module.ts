import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { BookpageComponent } from './components/bookpage/bookpage.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { AddbookComponent } from './components/addbook/addbook.component';
import {
  HttpClientInMemoryWebApiModule,
  InMemoryWebApiModule,
} from 'angular-in-memory-web-api';
import { DataService } from './services/data.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { EachbookComponent } from './components/eachbook/eachbook.component';
import { NgModel } from '@angular/forms';
import { ValidatorDirective } from './validation/validator.directive';
import { FooterComponent } from './components/footer/footer.component';
import { FilterBookCategoryPipe } from './filter-book-category.pipe';
import { NotFoundComponent } from './components/not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    BookpageComponent,
    NavbarComponent,
    LoginComponent,
    AddbookComponent,
    EachbookComponent,
    ValidatorDirective,
    FooterComponent,
    FilterBookCategoryPipe,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule,
    InMemoryWebApiModule.forRoot(DataService),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
