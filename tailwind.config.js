/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,ts}"],
  theme: {
    extend: {
      fontFamily: {
        Rubik: ["Rubik", "san-serif"],
      },
    },
  },
  plugins: [],
};
